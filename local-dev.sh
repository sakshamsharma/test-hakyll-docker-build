#!/usr/bin/env bash

if [ $# -eq 0 ]
  then
    echo "Docker name is required as the first argument."
    exit 1
fi

docker run --net=host -it -v $PWD/_site:/root/proj/_site -v $PWD/.stack-work:/root/proj/.stack-work $1 $2
