FROM sakshamsharma/docker-hakyll:v2

RUN mkdir -p /root/proj
WORKDIR /root/proj
ADD . /root/proj

ENTRYPOINT ["/root/proj/bob-the-builder.sh"]
CMD ["build"]
